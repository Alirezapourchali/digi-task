
## multistage

FROM golang:1.22-rc-alpine AS build

WORKDIR /app

COPY ./go.mod ./

RUN go mod download
COPY . .
RUN go build -o /godocker /app/main.go


## this might create difficulties in the future for troubleshooting !
## you could change the base image to alpine and copy the binary from the build stage
FROM scratch

COPY --from=build /godocker /godocker

EXPOSE 8080

CMD ["/godocker"]

## image name that i have pushed to dockerhub
# arpjoker/mygoapi