package main

import (
	"net/http"
	"os"

	"github.com/gin-gonic/gin"
)

func getNodeName(context *gin.Context) {
	context.IndentedJSON(http.StatusOK, os.Getenv("NODE_NAME"))
}

func main() {
	router := gin.Default()
	router.GET("/", getNodeName)
	router.Run()
}
